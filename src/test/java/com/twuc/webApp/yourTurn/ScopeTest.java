package com.twuc.webApp.yourTurn;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.NoUniqueBeanDefinitionException;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;

public class ScopeTest {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    void setUp() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp.yourTurn");
    }

    // 2.1 - 1
    @Test
    void test_InterfaceOneImpl_and_InterfaceOne() {
//        InterfaceOne interfaceOneBean = context.getBean(InterfaceOne.class);
//        InterfaceOne interfaceOneImplBean = context.getBean(InterfaceOneImpl.class);
//
//        assertSame(interfaceOneBean, interfaceOneImplBean);
//        NoUniqueBeanDefinitionException 得到同一个实例
//        为什么这个不能像 2.1 - 2 一样写
    }

    // 2.1 - 2
    @Test
    void test_InterfaceOneImpl_and_InterfaceOneImplSon() {
        context.getBean(InterfaceOneImplSon.class);

        assertThrows(NoUniqueBeanDefinitionException.class, () -> {
            context.getBean(InterfaceOneImpl.class);
        });
    }

    // 2.1 - 3
    @Test
    void test_DerivedClass_and_AbstractBaseClass() {
        AbstractBaseClass derivedClassBean = context.getBean(DerivedClass.class);
        AbstractBaseClass abstractBaseClassBean = context.getBean(AbstractBaseClass.class);

        assertSame(derivedClassBean, abstractBaseClassBean);
    }

    // 2.2 - 1
    @Test
    void test_SimplePrototypeScopeClass_with_prototype_scope() {
        SimplePrototypeScopeClass bean1 = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass bean2 = context.getBean(SimplePrototypeScopeClass.class);

        assertNotSame(bean1, bean2);
    }

    // 2.2 - 2
    /*
    singleton 扫描时创建
    prototype 是getBean()时创建
     */
    @Test
    void test_when_isinstance_is_created() {
        Prototype prototypeBean = context.getBean(Prototype.class);
        context.getBean(Prototype.class);
        List<String> logs = prototypeBean.getLogger().getLogs();
        List<String> singletonList = logs.stream().filter(log -> log.equals("singleton()")).collect(Collectors.toList());
        List<String> prototypeList = logs.stream().filter(log -> log.equals("prototype()")).collect(Collectors.toList());

        assertEquals(1,singletonList.size());
        assertEquals(2,prototypeList.size());
    }

    // 2.2 - 3
    @Test
    void should_get_singleton_object_using_getBean() {
        SingletonGetBean bean = context.getBean(SingletonGetBean.class);
        context.getBean(SingletonGetBean.class);
        List<String> logs = bean.getLogger().getLogs();
        List<String> singletonList = logs.stream().filter(log -> log.equals("singletonGetBean()")).collect(Collectors.toList());

        assertEquals(2,singletonList.size());
    }

    // 2.2 - 4
    @Test
    void should_create_one_singleton_and_create_two_prototype_when_prototype_dependency_singleton() {
        PrototypeScopeDependsOnSingleton bean = context.getBean(PrototypeScopeDependsOnSingleton.class);
        context.getBean(PrototypeScopeDependsOnSingleton.class);
        List<String> logs = bean.getLogger().getLogs();
        List<String> singletonDependent = logs.stream().filter(log -> log.equals("SingletonDependent")).collect(Collectors.toList());
        List<String> prototypeScopeDependsOnSingleton = logs.stream().filter(log -> log.equals("PrototypeScopeDependsOnSingleton")).collect(Collectors.toList());

        assertEquals(1, singletonDependent.size());
        assertEquals(2, prototypeScopeDependsOnSingleton.size());

    }

    // 2.2 - 5
    @Test
    void should_create_one_singleton_and_one_prototype_when_singleton_dependency_prototype() {
        SingletonDependsOnPrototype bean = context.getBean(SingletonDependsOnPrototype.class);
        context.getBean(SingletonDependsOnPrototype.class);
        List<String> logs = bean.getLogger().getLogs();
        List<String> prototypeDependent = logs.stream().filter(log -> log.equals("PrototypeDependent")).collect(Collectors.toList());
        List<String> singletonDependsOnPrototype = logs.stream().filter(log -> log.equals("SingletonDependsOnPrototype")).collect(Collectors.toList());

        assertEquals(1, prototypeDependent.size());
        assertEquals(1, singletonDependsOnPrototype.size());
    }

    // 2.3 - 1
    @Test
    void should_create_new_prototypeDependentWithProxy_when_call_field() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
        bean.getPrototypeDependentWithProxy().field();
        bean.getPrototypeDependentWithProxy().field();
        List<String> logs = bean.getLogger().getLogs();
        List<String> singletonDependsOnPrototypeProxy = logs.stream().filter(log -> log.equals("SingletonDependsOnPrototypeProxy")).collect(Collectors.toList());
        List<String> prototypeDependentWithProxy = logs.stream().filter(log -> log.equals("PrototypeDependentWithProxy")).collect(Collectors.toList());

        assertEquals(1,singletonDependsOnPrototypeProxy.size());
        assertEquals(2,prototypeDependentWithProxy.size());
    }

    // 2.3 - 2
    @Test
    void should_create_two_prototype_object_when_invoke_no_independent_implementation_method_twice() {
        SingletonDependsOnPrototypeProxyBatchCall bean = context.getBean(SingletonDependsOnPrototypeProxyBatchCall.class);
        bean.prototypeProxyNotImplementIndependent();
        List<String> logs = bean.getLogger().getLogs();
        List<String> singletonDependsOnPrototypeProxyBatchCall = logs.stream().filter(log -> log.equals("SingletonDependsOnPrototypeProxyBatchCall")).collect(Collectors.toList());
        List<String> prototypeDependentWithProxy = logs.stream().filter(log -> log.equals("PrototypeDependentWithProxy")).collect(Collectors.toList());

        assertEquals(1,singletonDependsOnPrototypeProxyBatchCall.size());
        assertEquals(2,prototypeDependentWithProxy.size());
    }

    // 2.3 - 3 不是很懂
    @Test
    void name() {
        SingletonDependsOnPrototypeProxy bean = context.getBean(SingletonDependsOnPrototypeProxy.class);
    }
}
