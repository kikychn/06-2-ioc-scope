package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class Singleton {
    private final MyLogger logger;

    public Singleton(MyLogger logger) {
        logger.log("singleton()");
        this.logger = logger;
    }

    public MyLogger getLogger() {
        return logger;
    }
}
