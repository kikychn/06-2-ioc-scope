package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependent {
    private final MyLogger logger;

    public MyLogger getLogger() {
        return logger;
    }

    public SingletonDependent(MyLogger logger) {
        logger.log("SingletonDependent");
        this.logger = logger;
    }
}
