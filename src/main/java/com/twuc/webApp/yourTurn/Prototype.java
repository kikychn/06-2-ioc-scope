package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class Prototype {
    private final MyLogger logger;

    public Prototype(MyLogger logger) {
        logger.log("prototype()");
        this.logger = logger;
    }

    public MyLogger getLogger() {
        return logger;
    }
}
