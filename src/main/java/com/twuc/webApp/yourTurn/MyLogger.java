package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MyLogger {
    private List<String> logs = new ArrayList<String>();

    public void log(String message) {
        logs.add(message);
    }

    public List<String> getLogs() {
        return logs;
    }
}
