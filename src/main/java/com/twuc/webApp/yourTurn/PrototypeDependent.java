package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrototypeDependent {
    private final MyLogger logger;

    public PrototypeDependent(MyLogger logger) {
        logger.log("PrototypeDependent");
        this.logger = logger;
    }

    public MyLogger getLogger() {
        return logger;
    }
}
