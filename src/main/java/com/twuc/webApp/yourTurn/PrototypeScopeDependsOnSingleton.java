package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class PrototypeScopeDependsOnSingleton {
    private SingletonDependent singletonDependent;
    private final MyLogger logger;

    public PrototypeScopeDependsOnSingleton(SingletonDependent singletonDependent, MyLogger logger) {
        this.singletonDependent = singletonDependent;
        logger.log("PrototypeScopeDependsOnSingleton");
        this.logger = logger;
    }

    public MyLogger getLogger() {
        return logger;
    }
}
