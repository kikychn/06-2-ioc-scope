package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import static org.springframework.context.annotation.ScopedProxyMode.TARGET_CLASS;

@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_PROTOTYPE,proxyMode = TARGET_CLASS)
public class PrototypeDependentWithProxy {
    private Integer id;
    private final MyLogger logger;

    public PrototypeDependentWithProxy(MyLogger logger) {
        logger.log("PrototypeDependentWithProxy");
        this.logger = logger;
    }

    public void field() {
    }

}
