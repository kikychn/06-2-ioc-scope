package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxyBatchCall {
    private PrototypeDependentWithProxy prototypeDependentWithProxy;
    private final MyLogger logger;

    public SingletonDependsOnPrototypeProxyBatchCall(PrototypeDependentWithProxy prototypeDependentWithProxy, MyLogger logger) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
        logger.log("SingletonDependsOnPrototypeProxyBatchCall");
        this.logger = logger;
    }

    public void prototypeProxyNotImplementIndependent() {
        this.getPrototypeDependentWithProxy().toString();
        this.getPrototypeDependentWithProxy().toString();
    }

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return prototypeDependentWithProxy;
    }

    public MyLogger getLogger() {
        return logger;
    }
}
