package com.twuc.webApp.yourTurn;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxy {
    private PrototypeDependentWithProxy prototypeDependentWithProxy;
    private final MyLogger logger;

    public SingletonDependsOnPrototypeProxy(PrototypeDependentWithProxy prototypeDependentWithProxy, MyLogger logger) {
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
        logger.log("SingletonDependsOnPrototypeProxy");
        this.logger = logger;
    }

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return prototypeDependentWithProxy;
    }

    public MyLogger getLogger() {
        return logger;
    }
}
