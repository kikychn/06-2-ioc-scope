package com.twuc.webApp.yourTurn;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototype {
    private PrototypeDependent prototypeDependent;
    private final MyLogger logger;

    public SingletonDependsOnPrototype(PrototypeDependent prototypeDependent, MyLogger logger) {
        this.prototypeDependent = prototypeDependent;
        logger.log("SingletonDependsOnPrototype");
        this.logger = logger;
    }

    public MyLogger getLogger() {
        return logger;
    }
}
